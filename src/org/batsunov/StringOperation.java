package org.batsunov;

public class StringOperation implements IStringOperation {


    @Override
    public String upEven(String str) {

        char[] chars = str.toCharArray();
        for (int i = 1; i < str.length(); i++) {

            if (i % 2 == 0 && chars[i] != ' ') {
                chars[i] = Character.toUpperCase(chars[i]);

            } else chars[i] = Character.toLowerCase(chars[i]);
        }

        return String.valueOf(chars);
    }

}



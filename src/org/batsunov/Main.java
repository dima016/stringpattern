package org.batsunov;

public class Main {
    public static void main(String[] args) {

        //1
        String str = "Hello World! My name is App.";
        String strUpEver = new StringOperation().upEven(str);

        System.out.println("Изначально было:" + str +
                "\nПосле поднятия:" + strUpEver +
                "\nПосле поднятия и реверса:" + new StringBuffer(strUpEver).reverse().toString() + '\n'
        );

        //2
        AddressParserIml address = new AddressParserIml("08606," +
                "Ивано-Франковская обл.," +
                "Васильківський р-н," +
                "С. ДЕРЕВ'ЯНКИ" +
                ",вул. Боженка" +
                ", 10/6 " +
                ",кв. 12");

        address.showAllInformation();

    }


}
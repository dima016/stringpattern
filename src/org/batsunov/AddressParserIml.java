package org.batsunov;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddressParserIml implements AddressParser {

    private String address;

    public AddressParserIml(String address) {
        this.address = address;
    }

    @Override
    public String getPostcode() {

        Pattern pattern = Pattern.compile("(\\d{5})");
        Matcher matcher = pattern.matcher(address);

        if (matcher.find()) {
            return matcher.group();
        }
        return null;
    }

    @Override
    public String getRegion() {

        Pattern pattern = Pattern.compile("([а-яА-ЯЇїҐґІі'\\-]+) (обл(\\.|асть))");
        Matcher matcher = pattern.matcher(address);

        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }

    @Override
    public String getDistrict() {
        Pattern pattern = Pattern.compile("([а-яА-ЯЇїҐґІіЄє'\\-]+) р(-н|айон)");
        Matcher matcher = pattern.matcher(address);

        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }

    @Override
    public String getPlace() {
        Pattern pattern = Pattern.compile("(Г.|С.|ПГТ.) ([А-ЯЇҐІЄ'\\-][А-ЯЇҐІЄ'\\-]+)");
        Matcher matcher = pattern.matcher(address);

        if (matcher.find()) {
            return matcher.group();
        }
        return null;
    }

    @Override
    public String getStreet() {
        Pattern pattern = Pattern.compile("(вул.|пров.) ([а-яА-ЯЇїҐґІіЄє'\\-]+)");
        Matcher matcher = pattern.matcher(address);

        if (matcher.find()) {
            return matcher.group();
        }
        return null;
    }


    @Override
    public String getRoom() {

        Pattern pattern = Pattern.compile("(кв.) (\\d+)");
        Matcher matcher = pattern.matcher(address);

        if (matcher.find()) {
            return matcher.group(2);
        }
        return null;
    }

    @Override
    public String getHouseNumber() {

        Pattern pattern = Pattern.compile("[^кв.][,|\\s]([0-9]{1,4}([а-яА-ЯЇїҐґІіЄє]|\\s|[\\/][0-9]+))");
        Matcher matcher = pattern.matcher(address);

        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }

    public void showAllInformation() {

        System.out.println("Почтовый индекс:" + getPostcode());
        System.out.println("Область:" + getRegion());
        System.out.println("Район:" + getDistrict());
        System.out.println("Населенный пункт:" + getPlace());
        System.out.println("Улица:" + getStreet());
        System.out.println("Номер дома:" + getHouseNumber());
        System.out.println("Номер квартиры:" + getRoom());

    }

}

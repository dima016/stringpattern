package org.batsunov;

public interface AddressParser {

    String getPostcode();

    String getRegion();

    String getDistrict();

    String getPlace();

    String getStreet();

    String getHouseNumber();

    String getRoom();
}
